#!/usr/bin/env bash

SHINIT="$HOME/.config/dot-shell"

# Make sure we use GNU ln on macos
if [ "$(uname -s)" = "Darwin" ]; then
    _gnu_ln=gln

    if [ ! -d "/opt/homebrew" ]; then
        # TODO: extend dot-shell to Intel macs, with homebrew at /usr/local
        echo "Warning: Homebrew is required at /opt/homebrew but is not found there."
    fi

    if ! type $_gnu_ln &>/dev/null; then
        echo "Error: GNU ln is not found. Install it with"
        echo "  $ brew install coreutils"
        echo "The following packages are all recommended:"
        echo "  $ brew install coreutils findutils gnu-sed grep"
        exit 1
    fi
else
    _gnu_ln=ln
fi

$_gnu_ln -s --backup=numbered "$SHINIT/bash_bashrc" "$HOME/.bashrc"
$_gnu_ln -s --backup=numbered "$SHINIT/bash_bash_profile" "$HOME/.bash_profile"
$_gnu_ln -s --backup=numbered "$SHINIT/zsh_zshrc" "$HOME/.zshrc"
$_gnu_ln -s --backup=numbered "$SHINIT/zsh_zprofile" "$HOME/.zprofile"
unset _gnu_ln
