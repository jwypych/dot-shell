import atexit
import code
import os
import readline

"""Hook into python startup to prevent spamming home dir with history file.

The python devs are bikeshedding XDG directory implementation, so this clumsy
workaround is necessary to prevent interactive python shells from writing to
~/.python_history, which is hard-coded in lib/python<version>/site.py.

This version allows concurrent interactive sessions to write history by only
appending the history file.
"""

if 'PYTHONHISTORY' in os.environ:
    history = os.path.expanduser(os.environ['PYTHONHISTORY'])
elif 'XDG_DATA_HOME' in os.environ:
    history = os.path.join(os.path.expanduser(os.environ['XDG_DATA_HOME']),
                           'python', 'python_history')
else:
    history = os.path.join(os.path.expanduser('~'),
                           '.python_history')

def initialize_history_file(history):
    open(history, 'wb').close()
    readline.add_history("# Start of history file.")
    readline.write_history_file(history)
    return readline.get_current_history_length()


history = os.path.abspath(history)
os.makedirs(os.path.dirname(history), exist_ok=True)

# If history file doesn't exist *or* is empty, Python will create ~/.python_history.
# In either case, create the file and seed it with a line of throwaway history.
try:
    readline.read_history_file(history)
    h_len = readline.get_current_history_length()
    if not h_len:
        initialize_history_file(history)
except FileNotFoundError:
    h_len = initialize_history_file(history)

def save(prev_h_len, history):
    new_h_len = readline.get_current_history_length()
    readline.set_history_length(1000)
    readline.append_history_file(new_h_len - prev_h_len, history)
atexit.register(save, h_len, history)
