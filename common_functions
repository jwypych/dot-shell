# -*- mode: sh -*- vim: ft=sh
#
# Functions common to bash and zsh

# Detect shell.
#  - Why? I want to define my primary utility functions like my_pathadd
#    here, but some variable handing is different in zsh and bash.
#  - This only works becasue this file is *sourced* by the bashrc and zshrc files.
_SHELL="unknown"
if [ -n "$BASH_VERSION" ]; then
    _SHELL="bash"
elif [ -n "$ZSH_VERSION" ]; then
    _SHELL="zsh"
fi

# Functions intended for shell config
my_pathadd () {
    # Checks for path existence and only adds to path variable if not there already.
    # VAR_NAME defaults to PATH if omitted.
    #   Usage: [VAR_NAME] path
    #   Examples:
    #     my_pathadd "/usr/bin/something"
    #     my_pathadd "FPATH" "/opt/homebrew/share/zsh-completions"
    if [ "$#" -eq 2 ]; then
        varname="$1"
        path_addition="$2"
    elif [ "$#" -eq 1 ]; then
        varname="PATH"
        path_addition="$1"
    else
        echo "my_pathadd accepts only 1 or 2 arguments."
        return 1
    fi

    if [ "$_SHELL" = "bash" ]; then
        # Indirection in bash: if varname="PATH", ${!varname} refers to $PATH
        [ -d "$path_addition" ] && case ":${!varname}:" in
            *:$path_addition:*) ;;
                 *) export "$varname=$path_addition${!varname:+:${!varname}}" ;;
        esac
    elif [ "$_SHELL" = "zsh" ]; then
        # Indirection in zsh: if varname="PATH", ${(P)varname} refers to $PATH
        [ -d "$path_addition" ] && case ":${(P)varname}:" in
            *:$path_addition:*) ;;
                 *) export "$varname=$path_addition${(P)varname:+:${(P)varname}}" ;;
        esac

    fi
}

my_source () { [ -e "$1" ] && . "$1"; } # Source file specified in $1, if it exists

my_export_path () {
    # Fancy exporter of a variable pointing to a directory or file.
    # Usage: my_export_path [mkdir] variable_name directory [file]
    #   1. If directory doesn't exist and $1 is "mkdir", create it; else return.
    #   2. If file is specified and does not exist, touch it.
    #   3. Use $HOME, not ~. The tilde doesn't expand as predictably.
    #   4. Example: "my_export_path mkdir MY_VAR $HOME/path/to/dir my_file" (my_file is optional)
    if [ "$1" = "mkdir" ]; then
        shift
        [ ! -d "$2" ] && mkdir -p "$2"
    else
        [ ! -d "$2" ] && return 1
    fi
    [ "$#" -eq 3 ] && [ ! -e "${2}/${3}" ] && touch "${2}/${3}" 2>/dev/null
    export $1="$2${3:+/${3}}"
}


# Functions intended for interactive use
catconf () {
    # Print file, skipping lines starting with # or ;
    if [ "$#" -eq 1 ] && [ -e "$@" ]; then
        grep -vxE '[[:blank:]]*([#;].*)?' "$@"
    else
        echo "Error: $@ is not a valid file."
        echo "Usage: catconf <file>"
    fi
}

pac-helper () {
    if [ "$#" -ne 1 ]; then
        echo "pac-helper performs routine pacman tasks. Commands:"
        echo "  drop-caches     clean up cache (leave one installed version)"
        echo "  update-mirrors  rate mirrors by speed and update mirrorlist"
        echo "  update-core     update mirrors, drop caches, upgrade system (no AUR)."
        echo "  update-all      update mirrors, drop caches, upgrade system (with AUR)."
        return 1
    fi
    case "$1" in
        drop-caches)
            command -v paccache &>/dev/null || { echo "No paccache found!" ; return 1 ; }
            sudo paccache -rk1
            sudo paccache -ruk0
            yay -Sc --aur --noconfirm
            ;;
        update-mirrors)
            command -v rate-mirrors &>/dev/null || { echo "No rate-mirrors found!" ; return 1 ; }
            export TMPFILE="$(mktemp)"
            sudo -v
            rate-mirrors --max-jumps 2 --country-test-mirrors-per-country 8 --country-neighbors-per-country 2 --save=$TMPFILE arch --path-to-test="extra/os/x86_64/extra.files" --max-delay=28000
            sudo mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist_backup
            sudo mv $TMPFILE /etc/pacman.d/mirrorlist
            ;;
        update-core)
            sudo -v
            pac-helper update-mirrors
            pac-helper drop-caches
            sudo pacman -Syyu
            ;;
        update-all)
            sudo -v
            pac-helper update-mirrors
            pac-helper drop-caches
            if command -v paru &> /dev/null; then
                paru -Syu
            elif command -v yay &> /dev/null; then
                yay -Syyu
            else
                echo "No AUR helper found, can't update AUR packages."
                return 1
            fi
            ;;
    esac
}

color () {
    if [ "$#" -lt 1 ]; then
        echo -e "Usage: color COMMAND [ARGUMENT]...\n"
        echo -e "This function adds a colorization argument and pages output with 'less -R'.\n"
        echo "Example:"
        echo "  color grep regexp .    runs 'grep --color=always regexp . | less -R'"
        return 1
    fi
    command="$1"
    shift
    case "$command" in
        # Specify exceptions first, with fallback to "standard" argument (ls, grep, etc.).
        rg)    c="--color always" ;;
        tree)  c="-C" ;;
        *)     c="--color=always" ;;
    esac
    eval "$command $c $@" | less -R --quit-if-one-screen --chop-long-lines
}

tssh () {
    # tmuxh ssh helper: rename pane to $1 and execute ssh $1
    tmux rename-window "$1"
    ssh "$1"
    tmux setw automatic-rename on
}

conda_initialize () {
    # Sets current shell session for conda use without having to run its crap
    # with every shell, every time. It's slow and shitty.
    __conda_setup="$('/opt/homebrew/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/opt/homebrew/anaconda3/etc/profile.d/conda.sh" ]; then
            . "/opt/homebrew/anaconda3/etc/profile.d/conda.sh"
        else
            export PATH="/opt/homebrew/anaconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
}

