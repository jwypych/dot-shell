This repository contains shell startup files for Bash and Zsh.

The intent is to use symlinks to refer to these files from the locations they
are expected to be. While this is not compliant with any relevant standards,
neither shell is completely compliant with the XDG Base Directory
specification, so some degree of messiness was unavoidable.


# Installation and Usage

Clone this repository to your user's `$HOME/.config` directory.

Review and optionally edit the file `xdg_base_dirs`. The environment
variables defined in this file determine the locations of the XDG
user cnofig, cache, and state directories.

Source `xdg_base_dirs` from the directory containing this repo:
```
  $ . xdg_base_dirs
```
Execute `setup_create_xdg_dirs.sh` from an arbitrary working directory to
create the XDG base directories, a `bash` subdirectory in `XDG_STATE_HOME`, and
`$HOME/.local/bin`. Directories are created with `mkdir -p`, which does nothing
if a directory already exists.

Execute `setup_create_symlinks.sh` from an arbitrary working directory to
create the necessary `$HOME` symlinks. Existing files will be saved with `.~#~`
suffixes, with `#` being replaced by serial numbers (in case of multiple
conflicts).

# Naming Convention

Each shell-specific file is prefixed with the name of the shell.  The remainder
of the filename follows that shell's convention, though all dots are omitted:

  - `bash_bash_profile` refers to the Bash file `.bash_profile`
  - `bash_bashrc` refers to the Bash file `.bashrc`

Files prefixed with `common_` are sourced by all shells, though in practice
this means only `bash` and `zsh`.

# Startup Files

Shells source various startup files depending on how the shells were invoked.
There are two main predicates: whether the shell is `interactive` and whether
it is a `login` shell.

As a result of the order of sourcing files, the `profile` files should contain
only those commands that need to be executed by login shells only, and should
source `.bashrc`/`.zshrc` for everything else.

Bash:
  - `~/.bash_profile` - sourced for interactive login shells
    + Actually, the rule is to source the first existing file of `.bash_profile`,
      `.bash_login`, or `.profile`.
  - `~/.bashrc` - sourced for interactive non-login shells
  - Note that Bash will source .profile when invoked with the name `sh`.

Zsh:
  - `~/.zshenv` - sourced for every zsh (unless /etc/zshenv is configured NOT
                to).  Export ZDOTDIR to change from `$HOME`. This is not being used by
                default because maintaining a separate `$HOME/.config/zsh` directory
                in addition to this `dot-shell` directory would be a bit opaque and
                over-complicated.
  - `$ZDOTDIR/.zprofile` - sourced for interactive login shells (before `.zshrc` if also
                         interactive)
  - `$ZDOTDIR/.zshrc` - sourced for interactive non-login shells
  - `$ZDOTDIR/.zlogin` - source for interactive login shells (after `.zshrc` if also
                       interactive)

# Sanitization of Repo and Instance Specificity

Each script initialization file sources a file with the same name as itself suffixed with
`_gitignored`. This pattern is defined in `.gitignore`.

This mechanism exists to permit instance-specific modifications and inclusion of private
data that will be ignored by `git`.

Example:
  - `bash_bashrc` sources `bash_bashrc_gitignored`

# XDG Base Directory Compliance

Bash does not and will not comply with the XDG base directory specification.  The best we
can do is to minimize the dot files that are needed:
  - `$HOME/.bash_profile` as a symlink to `$XDG_CONFIG_HOME/dot-shell/bash_bash_profile`
  - `$HOME/.bashrc` as a symlink to `$XDG_CONFIG_HOME/dot-shell/bash_bashrc`
  - `.bashrc` contains `export HISTFILE="$XDG_STATE_HOME"/bash/history`

Zsh also does not comply, though it has the ZDOTDIR mechanism. By default, the ZDOTDIR
mechanism is not used and we minimize to two the dot files that are needed in `$HOME`:
  - `$HOME/.zshrc` is a symlink to `$XDG_CONFIG_HOME/dot-shell/zsh_zshrc`
  - `$HOME/.zprofile` is a symlink to `$XDG_CONFIG_HOME/dot-shell/zsh_zprofile`

To use the ZDOTDIR mechanism:
  - Create `$HOME/.config/zsh`
  - Place the `.zshrc` and `.zprofile` symlinks there.
  - In `.zshenv`, `export ZDOTDIR="$HOME/.config/zsh` (remember, XDG vars not yet defined)
  - Create a symlink `$HOME/.zshenv` to `$XDG_CONFIG_HOME/dot-shell/zsh_zshenv`

# Deeper Dive on Startup File Sourcing Rules and Specifications

## Bash

Apparently the situation is more messy than the Bash manual suggests, but at a high level,
the following procedures are followed:

### Interactive login shell
  1. `/etc/profile`
  2. `$HOME/.bash_profile` OR `$HOME/.bash_login` OR `$HOME/.profile`
  3. On logout, `$HOME/.bash_logout`

### Interactive non-login shell
  1. `$HOME/.bashrc`

### Non-Interactive

This is invoked when it is started to run a script. None of the files above are sourced,
but the variable `$BASH_ENV` is expanded and sourced, if it exists.

### Invoked with the name 'sh'
  1. `/etc/profile`
  2. `$HOME/.profile`

### So where do we put stuff?

This ignores any symlinking for purposes of keeping shell initialization files together.

  1. `$HOME/.bash_profile` sources `$HOME/.bashrc`, which exits if being run
     non-interactively (before printing anything, else SSH/SCP may break!)
  2. If necessary to set login-specific initializations, specify those in
     `$HOME/.bash_profile`.
