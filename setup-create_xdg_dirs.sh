#!/usr/bin/env bash

# Bashism: extended test [[ to allow || inside braces and allow unquoted variables (safely).
if [[ -z $XDG_CONFIG_HOME || -z $XDG_CACHE_HOME || -z $XDG_DATA_HOME || -z $XDG_STATE_HOME ]]; then
    echo 'Error: XDG base directory variables not set, source $HOME/.config/dot-shell/xdg_base_dirs'
    echo '       and try again.'
    exit 1
fi

# Note: STATE/bash and .local/bin are not part of the XDG spec, but this config uses them.
_dirs=(
    "$XDG_CONFIG_HOME"
    "$XDG_CACHE_HOME"
    "$XDG_DATA_HOME"
    "$XDG_STATE_HOME/bash"
    "$HOME/.local/bin"
    )

echo "Safely creating (with \"mkdir -p\") the following:"
for d in ${_dirs[@]}; do
    echo "  $d"
    mkdir -p "$d"
done

unset _dirs
